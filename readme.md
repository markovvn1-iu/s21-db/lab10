# Lab 10

## Ex 1

![](.gitlab/ex1_t1.png)

![](.gitlab/ex1_t2.png)

Terminals show not the same lines because it is not possible to read dirty ceil with transaction isolation is set to 'read committed'

Two transactions read the same row of the table, then one transaction updates that row, and then the second transaction also updates the same row without taking into account the changes made by the first transaction. It is not possible with the current isolation level.

To prevent _lost update_ the second terminal was blacked untill the first terminal commit the transaction

## Ex 2.1

![](.gitlab/ex2.1_t1.png) 

![](.gitlab/ex2.1_t2.png)

## Ex 2.2

![](.gitlab/ex2.2_t1.png) 

![](.gitlab/ex2.2_t2.png)

Results are the same for both isolation levels. Balance of Bob does not change because the transaction cannot read dirty ceil

## Ex 3.1

![](.gitlab/ex3.1_t1.png) 

![](.gitlab/ex3.1_t2.png)

## Ex 3.2

![](.gitlab/ex3.2_t1.png) 

![](.gitlab/ex3.2_t2.png)

Isolation level 'serializable' is the most strict one. So, we got an error because with this isolation level it is not possible to access same cell concurrent from two transaction